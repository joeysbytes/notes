# Ansible Server v2

## Description

This is a complete re-write of the original Ansible Docker project, which has been archived.

## Features

* Runs as a Docker image, as 2 layers:
  1) This layer has all the software installed to run Ansible
  1) This layer extends the first layer with interactive software to run Ansible.
     * Create Vault Files
     * Run Playbook
     * DB of connections / passwords / ssh keys / vault files
* Pull playbooks directly from git repo?
* Handle ssh first-time connections

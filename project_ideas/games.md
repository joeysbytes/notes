# Game Ideas

## Core library for ANSI games.

* Perhaps part of EasyANSI widgets

## Kingdom of Kroz-like Game

* Need to really play through the games to get inspired.

## Lunar Lander

* Text mode or graphical?
* or for a challenge, do both!

## Pawns

* First person to get a pawn to the other side wins.
* Update to use full chess set options?
* Might be a good first project to learn how to make a computer opponent.

## Yahtzee-like clone with 12 dice

* This has been started, but is proving to be a really long game to play.
* High-level rules
  * Uses D20 dice
  * Roll a 1 - this die is out of use
  * Roll a 20 - this is a wild die
  * 2 - 7: Populates column 1
  * 8 - 13: Populates column 2
  * 14 - 19: Populates column 3
  * Can select number of dice to use, 5 - 12

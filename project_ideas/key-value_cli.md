# Key-Value-CLI

## Description

key-value is a python script that stores key/values in a json file. You can
do all maintenance of this database through commands. The idea is to use this
in other scripts like a command.

Project: [Key-Value CLI](https://gitlab.com/joeysbytes/key-value-cli)

## Features not added

* Add unit tests
* Add encrypted password functionality
* Publish to pypi ?

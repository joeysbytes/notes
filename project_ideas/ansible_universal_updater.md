# Universal Updater

The universal updater is an Ansible playbook that can inspect and update
a system.

## Features

* Package managers: apt, pacman, aur, snap, flatpak, appimage, zypper, deb, rpm, pkcon
* Ability to run custom scripts
* Handle specific software:
  * VirtualBox
  * Miniconda
  * Docker images
    * Stop, Start, Build

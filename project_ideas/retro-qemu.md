# Retro QEMU

## Description

A GUI to create / run DOS and older Windows virtual machines. (Python + tkinter)

## Features

* Show command being run to user
* Keep DB of user machines
* Have starting templates
* Support DOS 3.3 and up
* Floppy disks
  * Create images (360k, 720k, 1.2m, 1.44m, 2.88m)
  * Format image
  * Mount image
  * Unmount image
* Hard disks
  * Create images
  * Format image (FAT12, FAT16, FAT32)
  * Mount image
  * Unmount image
* Machine
  * Create / Load / Save / Run / Stop / Reboot / Delete / Snapshot
* Help System
  * System Info
    * List dependencies, and if they are found
    * Python version
    * QEMU version
    * qemu-system-i386 installed
    * tk installed / version
    * Pulseaudio version / installed
    * OS / version
  * About
* Virtual Hardware
  * Networking
  * Sound Card
  * Video Card
  * Hard Drives
  * Floppy Drives
  * CD Rom Drives
* Misc
  * Download FreeDOS for user, auto-install?
  * Have "Wizard" feature for creating a machine
  * Have templates for machines
    * DOS 3 - 4
    * DOS 5 - 6
    * Win 3.1
    * Win95, Win98, WinME
  * Keep Raspberry Pi OS in mind, but this has proven to be pretty low-powered for QEMU
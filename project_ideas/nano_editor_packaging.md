# nano Editor Packaging

## Description

Regardless of the operating system, have a way to use the latest and greatest
version of nano.  The options will be:

* As a Docker Image
  * Include configuration / syntax highlighting
* As a Flatpak
* As a Snap?
  * I hate this option because Snaps do not allow you to save outside your home directory.

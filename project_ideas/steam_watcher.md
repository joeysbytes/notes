# Steam Watcher

## Features

* Be able to automatically get list of all owned games / DLC.
* Watch for prices on wishlist items
* Keep track of unowned expansions, without having to add them to the wishlist.
* Publish daily report to a website.

# svr1 Items

## Features to add

* Smartdrive monitoring / alerting
* Storage space monitoring / alerting
* Auto-commit (git) certain projects every night
  * notes
  * config-files
* Spool directory cleanup
* MariaDB
* External Backups
  * GitHub
  * GitLab
  * GraciusGolf
  * RockholdLaw

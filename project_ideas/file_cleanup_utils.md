# File Cleanup Utilities

## Description

The primary purpose of these utilities is to examine a file system, and provide
insite in to what files are there, duplicate detection and removal, where space
is being used, etc.

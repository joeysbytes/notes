# Web Scraper / Downloader

* Keep copies of programming documentation available locally
  * MagPi Magazine and Books
  * Python Docs
  * Java Docs
* Computer drivers (BIOS)
* Linux distributions
  * Desktop / Server flavors
  * Ultimate Rescue CD
  * GParted
  * System Rescue CD
  * Raspberry PI OS
* GOG Games Downloader
* Humble Bundle eBooks Downloader

# The Cube Emulators

## Description

"The Cube" is a dedicated computer built from spare parts (mostly).
It is called The Cube due to the case it has been installed in to.
This computer was built to be able to be a "portable" retro machine
collection.

## Specs

| Category | Spec |
| --- | --- |
| Processor | 3rd Generation Intel i5 |
| RAM | 16 GB |
| Video | AMD Radeon 5450, 2 GB AMD (low-power) |
| OS | Manjaro 64-bit |

## Retro Computer Emulation

* DOS
  * qemu
  * DosBox
  * Try other emulators
* Apple II
* Apple IIGS
* Macintosh
* Commdore
  * VICE
  * PET / VIC-20 / C64 / C128
* Amiga

# Email cleanup processes

* Clean up emails based on configuration rules.  Examples:
  * Move an email only if it is read
  * Handle spam list that GMail is "missing"
  * Handle legitimate emails that GMail is sending to spam
  * Delete emails based on sender, rules, etc.

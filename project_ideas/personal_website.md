# Joey's Bytes Website

## Categories to Consider

* Recipes
* Board Game List
* Blog
* Books Being Read / Rating
* Links to Social Media: Facebook, YouTube, Twitch, Twitter, Goodreads
* DOS Color Chart (CGA / EGA)
* VGA Color Chart
* Video Game List
* Bookmarks page

## Misc

* Run minify on pages for bandwidth

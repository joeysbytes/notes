# Router Software

## Description

My Linksys router is capable of holding 2 different OS's, and booting from
either one.

I have run DD-WRT on it in the past, but upgrading it was always scary with
all the "this version could brick your router" warnings.

See if OpenWRT is a better option, or see if DD-WRT has improved since the
last time I tried it.

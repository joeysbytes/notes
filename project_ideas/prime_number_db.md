# Prime Number Database

## Description

A just for fun project, which can help with math-related projects, such as
Project Euler. Create a large database of prime numbers. Pre-load sets from
the internet, and then be able to expand on it with a generator.

# Bookmarks Website

## Description

Store bookmarks in formats that can be imported (Excel), and export to a
bookmarks website.

## Data

Each bookmark can hold:

* Text for Link
* Link
* Description (tooltip?)
* List of tags (categories), first tag = primary

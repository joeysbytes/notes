# Windows 10 Notes

[[_TOC_]]

## Create bootable Windows 10 Installation USB Flash Drive

TL;DR: Install [WoeUSB-ng](https://github.com/WoeUSB/WoeUSB-ng)

This is a Python GUI app which makes life super easy.

There are many sites which describe manual steps to make a bootable USB flash drive.  It is a lot of work, success is flaky, and even the brand of USB drive being used can be troublesome.  WoeUSB seems to get around everything, and it just works.

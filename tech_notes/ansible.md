# Ansible Notes

## Encrypt a string

```text
ansible-vault encrypt_string 'secretPassword' --name 'variable_name'
ansible-vault encrypt_string --vault-password-file a_password_file 'secretPassword' --name 'variable_name'
```

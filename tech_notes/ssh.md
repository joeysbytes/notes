# SSH

[[_TOC_]]

## Create SSH Keys

```bash
# ED25519 is the new, preferred, more secure type of key, and has a set length
ssh-keygen -t ed25519 -C "comment, commonly email address"

# RSA is more supported, replace -b with the size of the key you want
ssh-keygen -t rsa -b 4096 -C "comment, commonly email address"
```

## SSH Directory / File Permissions

| Component | Letter Permissions | Octal Permissions |
| --- | --- | --- |
| .ssh directory | rwx------ | 700 |
| private key | rw------- | 600 |
| public key | rw-r--r-- | 644 |
| known_hosts | rw------- | 600 |

## Use A Specific Key / known_hosts

```bash
# These key can be set permanently in ~/.ssh/config

ssh -i /path/to/key user@host
ssh -o UserKnownHostsFile=/path/to/known_hosts -i /path/to/key user@host
```

## Remove key from known_hosts

```bash
ssh-keygen -f "/home/user/.ssh/known_hosts" -R "host.name"
ssh-keygen -f "/home/user/.ssh/known_hosts" -R "1.2.3.4"
```

## Using SSHFS

When installing sshfs, you should add yourself to the *fuse* group, otherwise you have to be root to run it.

```bash
# To mount, there are options to map uids and gids
sshfs user@host:/path/to/server/directory /path/to/local/mount/directory

# To unmount, it's just like any other mounted directory
umount /path/to/local/mount/directory
```

## Using passwords in ssh commands in your scripts

When issueing an ssh (or scp) command in a bash script, you can install and utilize the sshpass command.
If you store the password in a file, you can avoid writing steps to read the value in to a variable.

```bash
# Copy file to remote server
sshpass -f "/data/credentials/password.txt" \
    scp "/tmp2/myfile.txt" "user@server:/home/user/"

# Run command on remote server
sshpass -f "/data/credentials/password.txt" \
        ssh "user@server" "/home/user/bin/myscript.sh"
```

## Common entries in /etc/ssh/sshd_config

| Variable | Known Valid Values | Notes |
| --- | --- | --- |
| AllowGroups | (any space-separated values) | Only users in these groups can ssh in. |
| AllowUsers | (any space-separated values) | Only users listed here can ssh in. |
| ChallengeResponseAuthentication | no<br />yes | Should be *no*  |
| DenyGroups | (any space-separated values) | Users in these groups can not ssh in. |
| DenyUsers | (any space-separated values) | Users listed here can not ssh in. |
| PasswordAuthentication | no<br />yes | |
| PermitRootLogin | no<br />prohibit-password<br />yes | |
| Port | (port number) | The port number sshd listens on, default = 22. |
